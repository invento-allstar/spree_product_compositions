class CreateSpreeProductPackages < ActiveRecord::Migration
  def change
    create_table :spree_product_packages do |t|
      t.timestamps null: false
    end
  end
end
