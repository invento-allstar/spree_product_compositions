class CreateSpreeProductPackageElements < ActiveRecord::Migration
  def change
    create_table :spree_product_package_elements do |t|
      t.references :product_package_id, index: true, foreign_key: true
      t.references :variant_id, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
