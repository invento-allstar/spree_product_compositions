class Spree::ProductPackageElement < ActiveRecord::Base
  belongs_to :product_package_id
  belongs_to :variant_id
end
