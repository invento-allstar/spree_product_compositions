Spree::Variant.class_eval do

	# many-to-many relationship to product packages;
	#+ a variant may be contained in many product packages 
	#+ and a product package may contain many variants
	has_many :product_package_elements
	has_many :product_packages, through: :product_package_elements


end