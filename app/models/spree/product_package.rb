class Spree::ProductPackage < ActiveRecord::Base

	# is a kind of product
	acts_as :product, as: :package, validates_actable: true

	# many-to-many relationship to variants;
	#+ a product package may contain many variants
	#+ and a variant may be contained in many product packages 
	has_many :product_package_elements
	has_many :variants, through: :product_package_elements

end
